const http = require('http');
const url = require('url');
const port = 3000;

const server = http.createServer((request, response) => {
	if (request.url == '/login') {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('You are in the login page.');
	} else {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("I'm sorry, the page you are looking for cannot be found.");
	}
});

server.listen(port);
console.log(`Server is successfully running.`);
