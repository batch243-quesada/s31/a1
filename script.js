// 1. What directive is used by Node.js in loading the modules it needs?

Answer: The required directive.


// 2. What Node.js module contains a method for server creation?

Answer: The http module contains the createServer() method that accepts a function as an argument and allows server creation.


// 3. What is the method of the http object responsible for creating a server using Node.js?

Answer: The createServer() Method


// 4. What method of the response object allows us to set status codes and content types?

Answer: The writeHead() Method.


// 5. Where will console.log() output its contents when run in Node.js?

Answer: To the terminal.


// 6. What property of the request object contains the address's endpoint?

Answer: The listen property.
